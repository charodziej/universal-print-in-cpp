# Examples

## basic.cpp

This is the most basic of all the examples. 

Like all the other projects using this library, it is based on the `template.cpp`. The first 26 lines are just that and don't have anything special about them.

Then we define all our test variables and call `watch()` on them afterwards. _simple_

### Linux

![basic.cpp on Linux](../screenshots/Linux_basic.png)

### Windows

![basic.cpp on Windows](../screenshots/Windows_basic.png)

## structs.cpp

This is a far more complex example.

Similarly to `basic.cpp` it's based on `template.cpp` and has the fancy things in its beginning.

This time though we have a struct with a special `print_process` method to allow our library to process it correctly.

This function first prints a '{' sign, then tells the library that the content should be indented from this point.

After that it prints all the members of the structure (using `cupl::print_process` to support all the types), followed by endlines and `cupl::indentation` to make the indentation look correctly.

Finally it informs the library that the content should no longer be indented and prints the closing '}'.

Then in the `int main()` it builds the custom list and calls `watch()` on its first element. _profit_

### Linux

![structs.cpp on Linux](../screenshots/Linux_structs.png)

### Windows

![structs.cpp on Windows](../screenshots/Windows_structs.png)

## math.cpp

This example show that you can debug arithmetic operations with this library.

It implements the Fourier series approximation and calculates it for some random values.

### Linux

![math.cpp on Linux](../screenshots/Linux_math.png)

### Windows

![math.cpp on Windows](../screenshots/Windows_math.png)

## trees.cpp

This example shows capabilities of printing binary trees. 

First 27 lines are just `template.cpp` and there is nothing special to tell about them.

#### Binary tree in array (a)

Here is the most basic binary tree and nothing fancy.
We also `watch()` this same example starting in a different root.

#### Binary tree in array (b)

This example shows that a binary tree can be used with all types of data including multi-dimensional structures.

#### Binary tree in array (c)

Here we can see that you can print binary trees with mixed data types and structures.

#### Binary tree in array (d)

This is an exposition of the true power of this library. For example, we can mirror the tree without adding anything.

#### Binary tree in array (e)

Here we're printing a tree without the left subtree.

#### Binary tree in array (f)

Lambdas will be working, I promise.

#### Binary tree in adjacency matrix (a)

Here we are constructing functions for other methods of handling binary trees.

### Linux

![trees.cpp on Linux](../screenshots/Linux_trees.png)

### Windows

![trees.cpp on Windows](../screenshots/Windows_trees.png)

## graph.cpp

To be seen!

## extreme.cpp

I don't think there is much to be said about this one. It is completely unrealistic and only shows how much potential this library has.

It may look pretty bad on some terminals but is definitely worth checking out.

_enjoy at your own risk_

### Linux

![extreme.cpp on Linux](../screenshots/Linux_extreme.png)

### Windows

![extreme.cpp on Windows](../screenshots/Windows_extreme.png)

## random.cpp

There is some random stuff we didn't want to delete because it has sentimental value for us.

### Linux

![random.cpp on Linux](../screenshots/Linux_random.png)

### Windows

![random.cpp on Windows](../screenshots/Windows_random.png)

